# cython: language_level=3, boundscheck=False, cdivision=True
import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef np.float_t DTYPE_t

cdef float _vector_to_angle(float x, float y):
	angle = np.arctan(y/x) * 180.0/np.pi
	if x < 0:
		angle += 180.0
	return angle

def get_B_and_arrows(
	field,
	loopPos,
	list xRange, list yRange,
	int gridSizeX, int gridSizeY,
	float minDist, float maxDist):
	
	xSpace = np.linspace(xRange[0], xRange[1], num=gridSizeX)
	ySpace = np.linspace(yRange[0], yRange[1], num=gridSizeY)
	Bx = np.full((gridSizeX, gridSizeY), 0.0)
	By = np.full((gridSizeX, gridSizeY), 0.0)
	Bz = np.full((gridSizeX, gridSizeY), 0.0)
	arrows = []
	
	cdef int ix
	cdef int iy
	cdef int iL
	
	for ix in range(gridSizeX):
		for iy in range(gridSizeY):
			distances = []
			for iL in range(len(loopPos)):
				distances.append(np.sqrt((xSpace[ix]-loopPos[iL][0])**2 + (ySpace[iy]-loopPos[iL][1])**2))
			tooClose = np.any([d < minDist for d in distances])
			tooFar = np.all([d > maxDist for d in distances])
			if tooClose or tooFar:
				continue
			Bx[ix,iy], By[ix,iy], Bz[ix,iy] = field.evaluate([xSpace[ix], ySpace[iy], 0.0])
			angle = _vector_to_angle(Bx[ix,iy], By[ix,iy]) + 180.0 # +180 b/c the arrows are reversed
			arrows.append((xSpace[ix], ySpace[iy], angle))
	B = np.sqrt(Bx**2 + By**2)
	return B, arrows

def get_xy_strength(
	field,
	list xRange, list yRange,
	int gridSizeX, int gridSizeY):
	
	xSpace = np.linspace(xRange[0], xRange[1], num=gridSizeX)
	ySpace = np.linspace(yRange[0], yRange[1], num=gridSizeY)
	Bx = np.full((gridSizeX,), np.nan)
	By = np.full((gridSizeY,), np.nan)
	
	cdef int ix
	cdef int iy
	
	for ix in range(gridSizeX):
		bx,by,bz = field.evaluate([xSpace[ix], 0.0, 0.0])
		Bx[ix] = np.sqrt(bx**2 + by**2)
	for iy in range(gridSizeY):
		bx,by,bz = field.evaluate([0.0, ySpace[iy], 0.0])
		By[iy] = np.sqrt(bx**2 + by**2)
	
	return Bx, By