#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
This module loads an interface for visualizing a pair of helmholtz coils
aligned along an axis.

Distributed under the MIT license:


Copyright (c) 2017 Jacob C. Laas

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""
# standard library
import sys
import os
from functools import partial
import time
import argparse
# third-party
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.Qt import uic
loadUiType = uic.loadUiType
import pyximport; pyximport.install(setup_args={'include_dirs': np.get_include()})
from matplotlib import cm
# local
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(sys.argv[0]))))
sys.path.insert(0, os.path.dirname(__file__))
import loopfield as lf
import Widgets
try:
	import miscfunctions
	from miscfunctions import get_B_and_arrows, get_xy_strength
except ImportError:
	print("WARNING: couldn't import the cython (i.e. C-compiled and a bit faster)",
	" version of some helper functions! will use a python-only version till",
	" this issue gets fixed.. the interface may be a bit slower for calculations..")
	
	def get_B_and_arrows(
		field,
		loopPos,
		xRange, yRange,
		gridSizeX, gridSizeY,
		minDist, maxDist):
		
		xSpace = np.linspace(xRange[0], xRange[1], num=gridSizeX)
		ySpace = np.linspace(yRange[0], yRange[1], num=gridSizeY)
		Bx = np.full((gridSizeX, gridSizeY), 0.0)
		By = np.full((gridSizeX, gridSizeY), 0.0)
		Bz = np.full((gridSizeX, gridSizeY), 0.0)
		arrows = []
		
		for ix in range(gridSizeX):
			for iy in range(gridSizeY):
				distances = []
				for iL in range(len(loopPos)):
					distances.append(np.sqrt((xSpace[ix]-loopPos[iL][0])**2 + (ySpace[iy]-loopPos[iL][1])**2))
				tooClose = np.any([d < minDist for d in distances])
				tooFar = np.all([d > maxDist for d in distances])
				if tooClose or tooFar:
					continue
				Bx[ix,iy], By[ix,iy], Bz[ix,iy] = field.evaluate([xSpace[ix], ySpace[iy], 0.0])
				angle = vector_to_angle(Bx[ix,iy], By[ix,iy]) + 180.0 # +180 b/c the arrows are reversed
				arrows.append((xSpace[ix], ySpace[iy], angle))
		B = np.sqrt(Bx**2 + By**2)
		return B, arrows
	
	def get_xy_strength(
		field,
		xRange, yRange,
		gridSizeX, gridSizeY):
		
		xSpace = np.linspace(xRange[0], xRange[1], num=gridSizeX)
		ySpace = np.linspace(yRange[0], yRange[1], num=gridSizeY)
		Bx = np.full((gridSizeX,), np.nan)
		By = np.full((gridSizeY,), np.nan)
		
		for ix in range(gridSizeX):
			bx,by,bz = field.evaluate([xSpace[ix], 0.0, 0.0])
			Bx[ix] = np.sqrt(bx**2 + by**2)
		for iy in range(gridSizeY):
			bx,by,bz = field.evaluate([0.0, ySpace[iy], 0.0])
			By[iy] = np.sqrt(bx**2 + by**2)
		
		return Bx, By



def vector_to_angle(x, y):
	angle = np.arctan(y/x) * 180.0/np.pi
	if x < 0:
		angle += 180.0
	return angle


# determine the correct containing the *.ui files
ui_path = ""
if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'helmholtz_coil_pair.ui')):
	ui_path = os.path.dirname(os.path.realpath(sys.argv[0]))
elif os.path.isfile(os.path.join(os.path.dirname(__file__), 'helmholtz_coil_pair.ui')):
	ui_path = os.path.dirname(__file__)
else:
	raise IOError("could not identify the *.ui files")

Ui_test, QDialog = loadUiType(os.path.join(ui_path, 'helmholtz_coil_pair.ui'))
class HelmholtzGUI(QDialog, Ui_test):
	"""
	blah
	"""
	def __init__(self,
			debugging=False,
			numLoops=None,
			current=None,
			radius=None, spacing=None,
			tilt1=None, tilt2=None,
			gridSizeX=None, gridSizeY=None,
			minDist=None, maxDist=None,
			noVectors=False):
		"""
		blah
		"""
		super(self.__class__, self).__init__()
		self.setupUi(self)
		self.setWindowTitle("Helmholtz Plotter")
		
		# button functionality
		self.btn_update.clicked.connect(self.updatePlot)
		self.btn_test.clicked.connect(self.test)
		self.btn_quit.clicked.connect(self.quit)
		
		# keyboard shortcuts
		self.keyShortcutQuit = QtGui.QShortcut(QtGui.QKeySequence("Esc"), self)
		self.keyShortcutQuit.activated.connect(partial(self.quit, confirm=False))
		self.keyShortcutClearX = QtGui.QShortcut(QtGui.QKeySequence("Delete"), self.plotfigure_strengthX)
		self.keyShortcutClearX.activated.connect(self.clearXLabels)
		self.keyShortcutClearY = QtGui.QShortcut(QtGui.QKeySequence("Delete"), self.plotfigure_strengthY)
		self.keyShortcutClearY.activated.connect(self.clearYLabels)
		
		# gui elements/containers
		self.text_numLoops.opts.update({"formatString":"%d", 'constStep':5, "min":1, "max":1e3})
		self.text_current.opts.update({"formatString":"%d", 'constStep':50, "min":1, "max":1e3})
		self.text_radius.opts.update({"formatString":"%.1f", 'constStep':5, "min":1, "max":2e2})
		self.text_spacing.opts.update({"formatString":"%.1f", 'constStep':5, "min":1, "max":2e2})
		self.text_tilt1.opts.update({"formatString":"%d", 'constStep':1, "min":-45, "max":45})
		self.text_tilt2.opts.update({"formatString":"%d", 'constStep':1, "min":-45, "max":45})
		self.plotTypes = ["|B| + vector", "|B| only", "vectors only"]
		for t in self.plotTypes:
			self.combo_plotType.addItem(t)
		if noVectors:
			self.combo_plotType.setCurrentIndex(1)
		
		# process inputs
		self.debugging = debugging
		if numLoops is not None:
			self.text_numLoops.setValue(numLoops)
		if current is not None:
			self.text_current.setValue(current)
		if radius is not None:
			self.text_radius.setValue(radius)
		if spacing is not None:
			self.text_spacing.setValue(spacing)
			self.check_lockSpacing.setChecked(False)
		if tilt1 is not None:
			self.text_tilt1.setValue(tilt1)
		if tilt2 is not None:
			self.text_tilt2.setValue(tilt2)
		if gridSizeX is not None:
			self.spinner_gridX.setValue(gridSizeX)
		if gridSizeY is not None:
			self.spinner_gridY.setValue(gridSizeY)
		if minDist is not None:
			self.spinner_minDist.setValue(minDist)
		if maxDist is not None:
			self.spinner_maxDist.setValue(maxDist)
		
		# slots/signals for sliders & text entries
		self.slider_numLoops.setRange(minInt=1, maxInt=1000, minFloat=1.0, maxFloat=1e3, sigRate=2)
		self.slider_numLoops.setValue(self.text_numLoops.value())
		self.activate_slider_and_text(self.slider_numLoops, self.text_numLoops)
		self.slider_current.setRange(minInt=0, maxInt=1000, minFloat=0.0, maxFloat=1e3, sigRate=2)
		self.slider_current.setValue(self.text_current.value())
		self.activate_slider_and_text(self.slider_current, self.text_current)
		self.slider_radius.setRange(minInt=1, maxInt=1000, minFloat=1.0, maxFloat=2e2, sigRate=2)
		self.slider_radius.setValue(self.text_radius.value())
		self.activate_slider_and_text(self.slider_radius, self.text_radius)
		self.slider_spacing.setRange(minInt=1, maxInt=1000, minFloat=1.0, maxFloat=2e2, sigRate=2)
		self.slider_spacing.setValue(self.text_spacing.value())
		self.activate_slider_and_text(self.slider_spacing, self.text_spacing)
		self.slider_tilt1.setRange(minInt=0, maxInt=900, minFloat=-45.0, maxFloat=45.0, sigRate=2)
		self.slider_tilt1.setValue(self.text_tilt1.value())
		self.activate_slider_and_text(self.slider_tilt1, self.text_tilt1)
		self.slider_tilt2.setRange(minInt=0, maxInt=900, minFloat=-45.0, maxFloat=45.0, sigRate=2)
		self.slider_tilt2.setValue(self.text_tilt2.value())
		self.activate_slider_and_text(self.slider_tilt2, self.text_tilt2)
		self.spinner_gridX.valueChanged.connect(self.updatePlot)
		self.spinner_gridY.valueChanged.connect(self.updatePlot)
		self.spinner_minDist.valueChanged.connect(self.updatePlot)
		self.spinner_maxDist.valueChanged.connect(self.updatePlot)
		
		# initializations
		self.init_plot()
		self.init_text()
		self.initFinished = True
	
	
	def init_plot(self):
		"""
		initializes the plot tab
		"""
		### fields tab
		self.plotfigure_fields.setLabel('left', "y-axis", units='m')
		self.plotfigure_fields.setLabel('bottom', "x-axis", units='m')
		self.plotfigure_fields.getPlotItem().vb.setAspectLocked()
		self.plotfigure_fields.getViewBox().setYRange(-0.5, 0.5)
		self.plotfigure_fields.getViewBox().setXRange(-0.5, 0.5)
		self.plotLegend_fields = self.plotfigure_fields.addLegend()
		self.plots_fields = []
		self.arrows_field = []
		self.img_field = None
		### mouse interaction for field plot
		self.plotMouseMovedField = pg.SignalProxy(
			self.plotfigure_fields.plotItem.scene().sigMouseMoved,
			rateLimit=5,
			slot=self.mouseMovedField)
		self.hoverLabelDotField = pg.TextItem(text="", anchor=(0.5,0.5))
		self.hoverLabelDotField.setZValue(0)
		self.hoverLabelTextField = pg.TextItem(text="", anchor=(0,0), fill=(0,0,0,100))
		self.hoverLabelTextField.setZValue(0)
		self.plotfigure_fields.addItem(self.hoverLabelDotField, ignoreBounds=True)
		self.plotfigure_fields.addItem(self.hoverLabelTextField, ignoreBounds=True)
		self.plotMouseClickedField = pg.SignalProxy(
			self.plotfigure_fields.plotItem.scene().sigMouseClicked,
			rateLimit=5,
			slot=self.mouseClickedField)
		self.labelsField = []
		### strength X & Y plots
		self.plotfigure_strengthX.setLabel('left', "field strength", units='T')
		self.plotfigure_strengthX.setLabel('bottom', "x-axis position", units='m')
		self.plotfigure_strengthX.setXLink(self.plotfigure_fields)
		self.plot_strengthX = self.plotfigure_strengthX.plot(name="strength along x")
		self.plotfigure_strengthY.setLabel('left', "y-axis position", units='m')
		self.plotfigure_strengthY.setLabel('bottom', "field strength", units='T')
		self.plotfigure_strengthY.setYLink(self.plotfigure_fields)
		self.plot_strengthY = self.plotfigure_strengthY.plot(name="strength along y")
		### mouse interaction for X
		self.plotMouseMovedX = pg.SignalProxy(
			self.plotfigure_strengthX.plotItem.scene().sigMouseMoved,
			rateLimit=15,
			slot=self.mouseMovedX)
		self.hoverLabelDotX = pg.TextItem(text="", anchor=(0.5,0.5))
		self.hoverLabelDotX.setZValue(0)
		self.hoverLabelTextX = pg.TextItem(text="", anchor=(0,0), fill=(0,0,0,100))
		self.hoverLabelTextX.setZValue(0)
		self.plotfigure_strengthX.addItem(self.hoverLabelDotX, ignoreBounds=True)
		self.plotfigure_strengthX.addItem(self.hoverLabelTextX, ignoreBounds=True)
		self.plotMouseClickedX = pg.SignalProxy(
			self.plotfigure_strengthX.plotItem.scene().sigMouseClicked,
			rateLimit=5,
			slot=self.mouseClickedX)
		self.labelsX = []
		### mouse interaction for Y
		self.plotMouseMovedY = pg.SignalProxy(
			self.plotfigure_strengthY.plotItem.scene().sigMouseMoved,
			rateLimit=15,
			slot=self.mouseMovedY)
		self.hoverLabelDotY = pg.TextItem(text="", anchor=(0.5,0.5))
		self.hoverLabelDotY.setZValue(0)
		self.hoverLabelTextY = pg.TextItem(text="", anchor=(0,0), fill=(0,0,0,100))
		self.hoverLabelTextY.setZValue(0)
		self.plotfigure_strengthY.addItem(self.hoverLabelDotY, ignoreBounds=True)
		self.plotfigure_strengthY.addItem(self.hoverLabelTextY, ignoreBounds=True)
		self.plotMouseClickedY = pg.SignalProxy(
			self.plotfigure_strengthY.plotItem.scene().sigMouseClicked,
			rateLimit=5,
			slot=self.mouseClickedY)
		self.labelsY = []
	
	def clearFieldLabels(self):
		for label in self.labelsField:
			self.plotfigure_fields.removeItem(label)
		self.labelsField = []
	def clearXLabels(self):
		for label in self.labelsX:
			self.plotfigure_strengthX.removeItem(label)
		self.labelsX = []
	def clearYLabels(self):
		for label in self.labelsY:
			self.plotfigure_strengthY.removeItem(label)
		self.labelsY = []
	
	def mouseMovedField(self, mouseEvent):
		try:
			test = self.field
		except:
			return
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			# convert mouse coordinates to XY wrt the plot
			mousePos = self.plotfigure_fields.plotItem.getViewBox().mapSceneToView(mouseEvent[0])
			mouseX, mouseY = mousePos.x(), mousePos.y()
			try:
				Bx,By,Bz = self.field.evaluate([mouseX, mouseY, 0.0])
				B = np.sqrt(Bx**2 + By**2)
				HTMLCoordinates = "<div style='text-align:left'><span style='font-size: 14pt'>"
				HTMLCoordinates += "position: %.1f, %.1f<br>B_x: %s<br>B_y: %s<br>|B|: %s</span></div>"
				self.hoverLabelDotField.setText("*")
				self.hoverLabelDotField.setPos(mouseX, mouseY)
				self.hoverLabelDotField.setZValue(999)
				self.hoverLabelTextField.setHtml(
					HTMLCoordinates % (
						mouseX*1000, mouseY*1000,
						pg.siFormat(Bx, suffix="T", precision=5),
						pg.siFormat(By, suffix="T", precision=5),
						pg.siFormat(B, suffix="T", precision=5)))
				self.hoverLabelTextField.setPos(mouseX, mouseY)
				self.hoverLabelTextField.setZValue(999)
			except:
				raise
		else:
			self.hoverLabelDotField.setText("")
			self.hoverLabelDotField.setPos(0,0)
			self.hoverLabelDotField.setZValue(0)
			self.hoverLabelTextField.setText("")
			self.hoverLabelTextField.setPos(0,0)
			self.hoverLabelTextField.setZValue(0)
	def mouseClickedField(self, mouseEvent):
		try:
			test = self.field
		except:
			return
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			pos = self.hoverLabelDotField.pos()
			dot = pg.TextItem(text="*", anchor=(0.5,0.5))
			dot.setPos(pos)
			dot.setZValue(999)
			self.plotfigure_fields.addItem(dot, ignoreBounds=True)
			self.labelsField.append(dot)
			text = pg.TextItem(text="", anchor=(0,0))
			text.setHtml(self.hoverLabelTextField.textItem.toHtml())
			text.setPos(pos)
			text.setZValue(999)
			self.plotfigure_fields.addItem(text, ignoreBounds=True)
			self.labelsField.append(text)
	
	def mouseMovedX(self, mouseEvent):
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			# convert mouse coordinates to XY wrt the plot
			mousePos = self.plotfigure_strengthX.plotItem.getViewBox().mapSceneToView(mouseEvent[0])
			mouseX, mouseY = mousePos.x(), mousePos.y()
			try:
				idx = np.abs(self.xSpace - mouseX).argmin()
				pos = self.xSpace[idx]
				B = self.Bx[idx]
				HTMLCoordinates = "<div style='text-align:left'><span style='font-size: 14pt'>"
				HTMLCoordinates += "<br>x-offset: %s<br>|B|: %s</span></div>"
				self.hoverLabelDotX.setText("*")
				self.hoverLabelDotX.setPos(pos, B)
				self.hoverLabelDotX.setZValue(999)
				self.hoverLabelTextX.setHtml(
					HTMLCoordinates % (
						pg.siFormat(pos, suffix="m"),
						pg.siFormat(B, suffix="T", precision=5)))
				self.hoverLabelTextX.setPos(pos, B)
				self.hoverLabelTextX.setZValue(999)
			except:
				raise
		else:
			self.hoverLabelDotX.setText("")
			self.hoverLabelDotX.setPos(0,0)
			self.hoverLabelDotX.setZValue(0)
			self.hoverLabelTextX.setText("")
			self.hoverLabelTextX.setPos(0,0)
			self.hoverLabelTextX.setZValue(0)
	def mouseClickedX(self, mouseEvent):
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			pos = self.hoverLabelDotX.pos()
			dot = pg.TextItem(text="*", anchor=(0.5,0.5))
			dot.setPos(pos)
			dot.setZValue(999)
			self.plotfigure_strengthX.addItem(dot, ignoreBounds=True)
			self.labelsX.append(dot)
			text = pg.TextItem(text="", anchor=(0,0))
			text.setHtml(self.hoverLabelTextX.textItem.toHtml())
			text.setPos(pos)
			text.setZValue(999)
			self.plotfigure_strengthX.addItem(text, ignoreBounds=True)
			self.labelsX.append(text)
	
	def mouseMovedY(self, mouseEvent):
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			# convert mouse coordinates to XY wrt the plot
			mousePos = self.plotfigure_strengthY.plotItem.getViewBox().mapSceneToView(mouseEvent[0])
			mouseX, mouseY = mousePos.x(), mousePos.y()
			try:
				idx = np.abs(self.ySpace - mouseY).argmin()
				pos = self.ySpace[idx]
				B = self.By[idx]
				HTMLCoordinates = "<div style='text-align:left'><span style='font-size: 14pt'>"
				HTMLCoordinates += "<br>y-offset: %s<br>|B|: %s</span></div>"
				self.hoverLabelDotY.setText("*")
				self.hoverLabelDotY.setPos(B, pos)
				self.hoverLabelDotY.setZValue(999)
				self.hoverLabelTextY.setHtml(
					HTMLCoordinates % (
						pg.siFormat(pos, suffix="m"),
						pg.siFormat(B, suffix="T", precision=5)))
				self.hoverLabelTextY.setPos(B, pos)
				self.hoverLabelTextY.setZValue(999)
			except:
				raise
		else:
			self.hoverLabelDotY.setText("")
			self.hoverLabelDotY.setPos(0,0)
			self.hoverLabelDotY.setZValue(0)
			self.hoverLabelTextY.setText("")
			self.hoverLabelTextY.setPos(0,0)
			self.hoverLabelTextY.setZValue(0)
	def mouseClickedY(self, mouseEvent):
		modifier = QtGui.QApplication.keyboardModifiers()
		if modifier == QtCore.Qt.ShiftModifier:
			pos = self.hoverLabelDotY.pos()
			dot = pg.TextItem(text="*", anchor=(0.5,0.5))
			dot.setPos(pos)
			dot.setZValue(999)
			self.plotfigure_strengthY.addItem(dot, ignoreBounds=True)
			self.labelsY.append(dot)
			text = pg.TextItem(text="", anchor=(0,0))
			text.setHtml(self.hoverLabelTextY.textItem.toHtml())
			text.setPos(pos)
			text.setZValue(999)
			self.plotfigure_strengthY.addItem(text, ignoreBounds=True)
			self.labelsY.append(text)
	
	
	def init_text(self):
		"""
		initializes the text tab
		"""
		font = QtGui.QFont()
		font.setFamily('Mono')
		self.textEdit.setFont(font)
	
	
	def test(self):
		"""
		runs temporary tests (for debugging only)
		"""
		### tests profiling the updatePlot() command
		runsnake('self.updatePlot()', globals=globals(), locals=locals())
		#### tests rotating vector angles
		#theta = np.radians(5)
		#c, s = np.cos(theta), np.sin(theta)
		#coord_orig_top = np.matrix([0, 1])
		#coord_orig_bot = np.matrix([0, -1])
		#coord_rot = np.matrix([[c, -s], [s, c]])
		#coord_new_top = coord_orig_top * coord_rot
		#coord_new_bot = coord_orig_bot * coord_rot
		#print coord_new_top.tolist()[0], coord_new_bot
		#### tests computing vector angles
		#def vector_to_angle(x, y):
		#	angle = np.arctan(y/x) * 180/3.14159
		#	if x < 0:
		#		angle += 180.0
		#	return angle
		#print "should be 45°: %s" % vector_to_angle(1.0, 1.0)
		#print "should be 135°: %s" % vector_to_angle(-1.0, 1.0)
		#print "should be 225°: %s" % vector_to_angle(-1.0, -1.0)
		#print "should be -45°: %s" % vector_to_angle(1.0, -1.0)
		#### tests adding an image and contour plot
		#x = np.linspace(0, 6.28, 30)
		#y = x[:]
		#xx,yy = np.meshgrid(x, y)
		#z = np.sin(xx) + np.cos(yy)
		#print z.shape
		#print z[:2]
		#vb = self.plotWidget.getPlotItem().vb
		#vb.setAspectLocked()
		#img = pg.ImageItem(z)
		#img.setZValue(10)
		#vb.addItem(img)
		#c = pg.IsocurveItem(data=z, level=1, pen='r')
		#c.setParentItem(img)
		#c.setZValue(90)
		#vb.addItem(c)
		#### tests a couple test cases and updates the text tab
		## move cursor to the end, and update its contents
		#self.textEdit.clear()
		#cursor = self.textEdit.textCursor()
		#cursor.movePosition(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)
		#self.textEdit.setTextCursor(cursor)
		## define new class, and run two tests
		#myLoop = helmholtz_pair()
		#myLoop.test_single_loop()
		#result = myLoop.test_pair()
		#self.textEdit.insertPlainText(result)
	
	
	def updatePlot(self):
		### collect parameters
		numLoops = self.text_numLoops.value()
		current = self.text_current.value() * numLoops * 1e-3
		radius = self.text_radius.value() * 1e-2
		if self.check_lockSpacing.isChecked():
			spacing = radius
		else:
			spacing = self.text_spacing.value() * 1e-2
		tilt1 = self.text_tilt1.value()
		tilt2 = self.text_tilt2.value()
		gridSizeX = self.spinner_gridX.value()
		gridSizeY = self.spinner_gridY.value()
		minDist = self.spinner_minDist.value() * 1e-2
		maxDist = self.spinner_maxDist.value() * 1e-2
		### clear plots
		vb_fields = self.plotfigure_fields.getPlotItem().vb
		vb_strengthX = self.plotfigure_strengthX.getPlotItem().vb
		vb_strengthY = self.plotfigure_strengthY.getPlotItem().vb
		if self.img_field is not None:
			vb_fields.removeItem(self.img_field)
		for p in self.plots_fields:
			self.plotfigure_fields.removeItem(p)
			self.plotLegend_fields.removeItem(p.name())
		self.plots_fields = []
		for a in self.arrows_field:
			self.plotfigure_fields.removeItem(a)
		self.arrows_field = []
		### add loops
		loopPos = []
		# first loop
		theta1 = np.radians(tilt1)
		c, s = np.cos(theta1), np.sin(theta1)
		rotmat1 = np.matrix([[c, -s], [s, c]])
		loopXY1out = np.matrix([0.0, radius]) * rotmat1
		loopXY1in = np.matrix([0.0, -radius]) * rotmat1
		loopXY1out = loopXY1out.tolist()[0]
		loopXY1in = loopXY1in.tolist()[0]
		loopXY1out[0] -= spacing/2.0
		loopXY1in[0] -= spacing/2.0
		p1 = self.plotfigure_fields.plot(
			x=[loopXY1out[0]], y=[loopXY1out[1]], symbol='o', symbolSize=14,
			pen=(0,0,200), symbolBrush=(0,0,200), symbolPen='w')
		p1.setZValue(20)
		self.plots_fields.append(p1)
		p2 = self.plotfigure_fields.plot(
			x=[loopXY1in[0]], y=[loopXY1in[1]], symbol='x', symbolSize=14,
			pen=(0,0,200), symbolBrush=(0,0,200), symbolPen='w')
		p2.setZValue(20)
		self.plots_fields.append(p2)
		p3 = self.plotfigure_fields.plot(
			x=[loopXY1out[0], loopXY1in[0]], y=[loopXY1out[1], loopXY1in[1]], name="Loop1",
			symbol='o', symbolSize=20, pen=(0,0,200), symbolBrush=(0,0,200,0), symbolPen='w')
		p3.setZValue(20)
		self.plots_fields.append(p3)
		loopPos += [loopXY1out, loopXY1in]
		# second loop
		theta2 = np.radians(tilt2)
		c, s = np.cos(theta2), np.sin(theta2)
		rotmat2 = np.matrix([[c, -s], [s, c]])
		loopXY2out = np.matrix([0.0, radius]) * rotmat2
		loopXY2in = np.matrix([0.0, -radius]) * rotmat2
		loopXY2out = loopXY2out.tolist()[0]
		loopXY2in = loopXY2in.tolist()[0]
		loopXY2out[0] += spacing/2.0
		loopXY2in[0] += spacing/2.0
		p4 = self.plotfigure_fields.plot(
			x=[loopXY2out[0]], y=[loopXY2out[1]], symbol='o', symbolSize=14,
			pen=(0,128,0), symbolBrush=(0,128,0), symbolPen='w')
		p4.setZValue(20)
		self.plots_fields.append(p4)
		p5 = self.plotfigure_fields.plot(
			x=[loopXY2in[0]], y=[loopXY2in[1]], symbol='x', symbolSize=14,
			pen=(0,128,0), symbolBrush=(0,128,0), symbolPen='w')
		p5.setZValue(20)
		self.plots_fields.append(p5)
		p6 = self.plotfigure_fields.plot(
			x=[loopXY2out[0], loopXY2in[0]], y=[loopXY2out[1], loopXY2in[1]], name="Loop2",
			symbol='o', symbolSize=20, pen=(0,128,0), symbolBrush=(0,128,0,0), symbolPen='w')
		p6.setZValue(20)
		self.plots_fields.append(p6)
		loopPos += [loopXY2out, loopXY2in]
		### define field and add loops
		self.field = lf.Field(
			length_units = lf.m,
			current_units = lf.A,
			field_units = lf.T)
		normal1 = np.matrix([1.0, 0.0]) * rotmat1
		normal1 = normal1.tolist()[0] + [0.0]
		loop1 = lf.Loop([-spacing/2.0, 0., 0.],
			normal1, radius, current)
		self.field.addLoop(loop1)
		normal2 = np.matrix([1.0, 0.0]) * rotmat2
		normal2 = normal2.tolist()[0] + [0.0]
		loop2 = lf.Loop([spacing/2.0, 0., 0.],
			normal2, radius, current)
		self.field.addLoop(loop2)
		### define grid
		xRange, yRange = vb_fields.viewRange()
		self.xSpace = np.linspace(xRange[0], xRange[1], num=gridSizeX)
		self.ySpace = np.linspace(yRange[0], yRange[1], num=gridSizeY)
		### set up fields plot
		B, arrows = get_B_and_arrows(self.field, loopPos, xRange, yRange, gridSizeX, gridSizeY, minDist, maxDist)
		if self.combo_plotType.currentIndex() in [0,2]:
			for a in arrows:
				arrow = pg.ArrowItem(angle=a[2], tipAngle=30, baseAngle=20, headLen=10, tailLen=None, brush=None)
				arrow.setPos(a[0], a[1])
				arrow.setZValue(20)
				self.arrows_field.append(arrow)
				self.plotfigure_fields.addItem(arrow)
		if self.combo_plotType.currentIndex() in [0,1]:
			viewrect_fields = vb_fields.viewRect()
			self.img_field = pg.ImageItem(B)
			self.img_field.setZValue(0)
			colormap = cm.get_cmap("nipy_spectral")
			colormap._init()
			lut = (colormap._lut * 255).view(np.ndarray)
			self.img_field.setLookupTable(lut)
			self.img_field.setRect(viewrect_fields)
			vb_fields.addItem(self.img_field)
		if self.check_clearFieldLabels.isChecked():
			self.clearFieldLabels()
		### set up field strength plots
		self.Bx, self.By = get_xy_strength(self.field, xRange, yRange, gridSizeX, gridSizeY)
		self.plot_strengthX.setData(x=self.xSpace, y=self.Bx)
		self.plot_strengthY.setData(x=self.By, y=self.ySpace)
	
	
	def activate_slider_and_text(self, slider, text):
		slider.valueChanged.connect(partial(self.updateTextFromSlider, slider, text))
		text.valueChanged.connect(partial(self.updateSliderFromText, slider, text))
		text.slowChangeProxy = pg.SignalProxy(
			text.valueChanged,
			slot=text.slowChange,
			rateLimit=10)
		text.valueChangedSlow.connect(self.updatePlot)
	
	def updateTextFromSlider(self, slider, text):
		text.setValue(slider.value())
	
	def updateSliderFromText(self, slider, text):
		slider.setValue(text.value())
	
	
	def quit(self, confirm=False):
		"""
		Provides the routine that quits the GUI.
		
		For now, simply quits the running instance of Qt.
		
		:param confirm: (optional) whether to first use a confirmation dialog
		:type confirm: bool
		"""
		# first invoke the confirmation dialog if necessary
		if confirm:
			msg = "Are you sure you want to exit the program?"
			response = QtGui.QMessageBox.question(self, "Confirmation", msg,
				QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
			if response == QtGui.QMessageBox.No:
				return
		# finally quit
		QtCore.QCoreApplication.instance().quit()


def runsnake(command, globals=None, locals=None):
	r"""
	Loads the RunSnakeRun graphical profiler
	
	Note that `runsnake` requires the program ``runsnake``. On Ubuntu,
	this can be done with:
		> sudo apt-get install python-profiler python-wxgtk2.8
		> sudo pip install RunSnakeRun
	See the ``runsnake`` website for instructions for other platforms.
	
	`runsnake` further assumes that the system wide Python is
	installed in ``/usr/bin/python``.
	
	see also:
		- `The runsnake website <http://www.vrplumber.com/programming/runsnakerun/>`
		- ``%prun``
	
	:param command: the desired command to run, including the parentheses
	:type command: str
	"""
	if not isinstance(command, str):
		raise SyntaxError("the desired command should be a string")
	import cProfile, tempfile
	fd, tmpfile = tempfile.mkstemp()
	os.close(fd)
	print("profiling command:\n\t'%s'" % (command))
	print("to file:\n\t'%s'"% (tmpfile))
	print("and launching it with 'runsnakerun':\n\t/usr/bin/python -E `which runsnake` %s &" % (tmpfile))
	cProfile.runctx(command.lstrip().rstrip(), globals=globals, locals=locals, filename=tmpfile)
	try:
		os.system("/usr/bin/python -E `which runsnake` %s &" % tmpfile)
	except:
		print("there was a problem calling runsnake!")





if __name__ == '__main__':
	# define GUI elements
	qApp = QtGui.QApplication(sys.argv)
	
	# define CLI input arguments
	description = """
	Calculates and plots the strength and direction of the magnetic field from a
	1-axis Helmholtz coil.
	
	For the purpose of axes/directions, the loops are positioned such that their
	center is along the zero of the x-axis, and offset equidistant from the
	y-axis, and the current is flowing in a clockwise manner if you look positive
	along the x-axis.
	
	The magnetic field at y=0.0 therefore travels along the x-axis, and follows
	the 'right-hand rule' if you point your thumb to the right.	That is, imagine
	you are holding out your right hand palm up and curl your fingers up. The
	current	follows your fingers to the tips (and around and around..).
	"""
	parser = argparse.ArgumentParser(description=description)
	parser.add_argument(
		"-d", "--debugging", action='store_true',
		help="whether to add extra print messages to the terminal")
	parser.add_argument("--numloops", type=int, help="how many loops to use (default: 100)")
	parser.add_argument("--current", type=int, help="what current (mA) to use (default: 200)")
	parser.add_argument("--radius", type=float, help="what radius (cm) to use (default: 50)")
	parser.add_argument("--spacing", type=float, help="what spacing (cm) to use, if not the same as the radius (default: 50)")
	parser.add_argument("--tilt1", type=int, help="what angle (deg) to use for the loop on the left wrt the y-axis (default: 0)")
	parser.add_argument("--tilt2", type=int, help="what angle to use for the loop on the right")
	parser.add_argument("--gridsizex", type=int, help="the number of grid points along the x-axis (default: 50)")
	parser.add_argument("--gridsizey", type=int, help="the number of grid points along the y-axis (default: 50)")
	parser.add_argument("--mindist", type=int, help="the minimum distance (cm) from a loop before calculating the field, to avoid high values for the contour plot (default: 5)")
	parser.add_argument("--maxdist", type=int, help="the maximum distance (cm) away from to calculating the field (default: 70)")
	parser.add_argument("--novectors", action='store_true', help="do not show vector arrows on the main field plot (better performance)")
	
	# parse arguments
	args = parser.parse_args()
	if args.debugging:
		print("CLI args were: %s" % args)
	
	# start GUI
	mainGUI = HelmholtzGUI(
		debugging=args.debugging,
		numLoops=args.numloops,
		current=args.current,
		radius=args.radius,
		spacing=args.spacing,
		tilt1=args.tilt1,
		tilt2=args.tilt2,
		gridSizeX=args.gridsizex,
		gridSizeY=args.gridsizey,
		minDist=args.mindist,
		maxDist=args.maxdist,
		noVectors=args.novectors)
	mainGUI.show()
	qApp.exec_()
	qApp.deleteLater()
	sys.exit()
