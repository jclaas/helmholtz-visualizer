#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
This module provides several classes related to simple widgets.

Copyright (c) 2017 pyLabSpec Development Team
Distributed under the 3-clause BSD license. See LICENSE for more infomation.
"""
# standard library
import os, sys, re
import time, datetime
import distutils.version
from functools import partial
# third-party
try:
	import pyqtgraph as pg
except ImportError as e:
	msg = "Could not import pyqtgraph!"
	if 'anaconda' in str(sys.version).lower():
		msg += "\n\tTry doing something like 'conda install pyqtgraph'\n"
	elif sys.platform == 'darwin':
		msg += "\n\tTry doing something like 'sudo port install py-pyqtgraph'\n"
	else:
		msg += "\n\tTry doing something like 'sudo pip install pyqtgraph'\n"
	raise ImportError(msg)
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.Qt import uic
import pyqtgraph.opengl as gl
loadUiType = uic.loadUiType
import numpy as np
# local
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(sys.argv[0]))))
sys.path.insert(0, os.path.dirname(__file__))

class ScrollableText(QtGui.QLineEdit):
	"""
	Provides a subclass of the QLineEdit field, so that one can use it
	with numbers, and scroll (via up/down or mouse-wheel) to change it
	by some constant value, or relative percentage.
	
	Note: this was stolen from pyLabSpec, and changes/bugfixes should
	also go there!
	"""
	valueChanged = QtCore.Signal(float)
	valueChangedSlow = QtCore.Signal(float)
	def __init__(self, parent, **opts):
		"""
		Initializes the object.
		
		:param parent: the parent window/widget containing the widget
		:type parent: QWidget
		"""
		super(self.__class__, self).__init__(parent)
		self.opts = dict(
			constStep = None,
			relStep = 10,
			formatString = "%.2e",
			min = None,
			max = None,
			slowRate = 2,
		)
		self.opts.update(opts)
	
	def keyPressEvent(self, event):
		"""
		Hijacks all keypresses, and checks if they are an up or down
		arrow. For some reason, the standard way of assigning a QShortcut
		doesn't work for.
		"""
		if event.key() == QtCore.Qt.Key_Up:
			self.increaseValue()
		elif event.key() == QtCore.Qt.Key_Down:
			self.decreaseValue()
		elif event.key() == QtCore.Qt.Key_Return:
			value = float(str(self.text()))
			self.setValue(value)
			self.valueChanged.emit(value)
		else:
			super(self.__class__, self).keyPressEvent(event)
	
	def increaseValue(self):
		"""
		Increases the current value. The increase depends on the option
		that has been set, defaulting first to the constant step.
		"""
		if not (self.opts['constStep'] or self.opts['relStep']):
			return
		try:
			value = float(str(self.text()))
			if self.opts['constStep']:
				value += self.opts['constStep']
			else:
				value *= (1 + self.opts['relStep']*0.01)
			if (self.opts['max'] is not None) and (value > self.opts['max']):
				value = self.opts['max']
			self.setText(self.opts['formatString'] % value)
			self.valueChanged.emit(value)
		except ValueError:
			pass
		except TypeError:
			pass
	
	def decreaseValue(self):
		"""
		Decreases the current value. The decrease depends on the option
		that has been set, defaulting first to the constant step.
		"""
		if not (self.opts['constStep'] or self.opts['relStep']):
			return
		try:
			value = float(str(self.text()))
			if self.opts['constStep']:
				value -= self.opts['constStep']
			else:
				value *= (1 - self.opts['relStep']*0.01)
			if (self.opts['min'] is not None) and (value < self.opts['min']):
				value = self.opts['min']
			self.setText(self.opts['formatString'] % value)
			self.valueChanged.emit(value)
		except ValueError:
			pass
		except TypeError:
			pass
	
	def wheelEvent(self,event):
		"""
		Invoked whenever the element catches a mouse-wheel event. In
		this case, it just calls increaseValue() or decreaseValue(),
		depending on the direction of the wheel movement.
		"""
		# bug fix: interpret the scroll direction according to the Qt version
		if distutils.version.LooseVersion(pg.Qt.QtVersion) > "5":
			delta = event.angleDelta()
			delta = int(delta.y())
		else:
			delta = event.delta()
		# carry on...
		if delta > 0:
			self.increaseValue()
		else:
			self.decreaseValue()
	
	def value(self):
		"""
		Returns the current value as an interpreted floating-point value.
		This provides compatibility for a pain-free replacement of a
		spinbox or doublespinbox.
		"""
		try:
			return float(str(self.text()))
		except ValueError:
			return None
		except TypeError:
			return None
	
	def setValue(self, value):
		"""
		Allows the setting of a value via a number. This provides
		additional compatibility with a spinbox or doublespinbox.
		
		:param value: the new value to be set
		:type value: str or float
		"""
		if (self.opts['min'] is not None) and (value < self.opts['min']):
			value = self.opts['min']
		elif (self.opts['max'] is not None) and (value > self.opts['max']):
			value = self.opts['max']
		try:
			self.setText(self.opts['formatString'] % float(value))
		except ValueError:
			return None
		except TypeError:
			return None
	
	def slowChange(self):
		"""
		Method that is called immediately whenever the slider's value is
		changed, but at a user-defined rate. Then it emits the rate-limited
		signal.
		"""
		self.valueChangedSlow.emit(self.value())


class DoubleSlider(QtGui.QSlider):
	"""
	Provides a special type of slider that converts the integer	location
	to an interpreted-as-a-double slider.
	
	Also, this has two new features: 1) an internal variable is used to
	keep track of change activity, where bool(self.isChanging) reflects
	this activity, but is reset after a defined delay (see self.setRange);
	and, 2) a new signal (self.valueChangedSlow) can also be used to keep
	track of a rate-limited signal much like self.valueChanged except with
	a rate (s^-1) defined by the user (also see self.setRange).
	
	Note: this was stolen from pyLabSpec, and changes/bugfixes should
	also go there!
	"""
	valueChangedSlow = QtCore.Signal(float)
	valueChangedDelayed = QtCore.Signal(float)
	def __init__(self, parent):
		"""
		Initializes the DoubleSlider object.
		
		:param parent: the parent window/widget containing the slider
		:type parent: QWidget
		"""
		QtGui.QSlider.__init__(self, parent)
		self.setRange()
	def setRange(
		self,
		minInt=0, maxInt=10000,
		minFloat=0.0, maxFloat=100.0,
		sigDelay=0.5, sigRate=10):
		"""
		Sets the lower/upper limits (both the standard integer type, as
		well as the floating-point type) of the slider, and re-initializes
		its states and slots.
		
		Note that the wisest use of the DoubleSlider is to set the int/
		float values so that each is off by the other by a specific factor
		of 10, which makes it easy to define the desired number of decimal
		digits of the float.
		
		:param minInt: (optional) minimum integer in range (default=0)
		:param maxInt: (optional) maximum integer in range (default=10000)
		:param minFloat: (optional) maximum float in range (default=0.0)
		:param maxFloat: (optional) maximum float in range (default=100.0)
		:param sigDelay: (optional) delay before the 'settled' signal is emitted, in unit [s] (default=0.5)
		:param sigRate: (optional) rate for emitting the slower signal, in unit [s^-1] (default=10)
		:type minInt: int
		:type maxInt: int
		:type minFloat: float
		:type maxFloat: float
		:type sigDelay: float
		:type sigRate: int
		"""
		# define limits (ints and floats)
		self.setMinimum(minInt)
		self.setMaximum(maxInt)
		self.minFloat = minFloat
		self.maxFloat = maxFloat
		# initialize delay-related things
		self.sigDelay = sigDelay
		self.sigRate = sigRate
		self.isChanging = False
		# define normal, 'instantaneous' connection
		self.instantChangeProxy = pg.SignalProxy(
			self.valueChanged,
			slot=self.instantChange)
		# define connection that always lags behind
		self.delayedChangeProxy = pg.SignalProxy(
			self.valueChanged,
			slot=self.delayedChange,
			delay=self.sigDelay)
		# define a rate-limited signal to connect to valueChangedSlow
		self.slowerChangeProxy = pg.SignalProxy(
			self.valueChanged,
			slot=self.slowChange,
			rateLimit=self.sigRate)
	def floatRange(self):
		"""
		Returns the total range of the min/max float values.
		
		:returns: the total range of the min/max float values
		:rtype: float
		"""
		return self.maxFloat-self.minFloat
	def value(self):
		"""
		Returns the current value of the slider.
		
		:returns: the current value
		:rtype: float
		"""
		value = float(super(self.__class__, self).value())
		value *= self.floatRange()/self.maximum()
		value += self.minFloat
		return value
	def setValue(self, value, delayedSignal=True):
		"""
		Used to set a new value to the slider.
		
		:param value: the new value
		:param delayedSignal: (optional) whether to also keep track of a delayed state
		:type value: float
		:type delayedSignal: bool
		"""
		value -= self.minFloat
		value *= self.maximum()/self.floatRange()
		value = int(value)
		super(self.__class__, self).setValue(value)
	def instantChange(self, sig):
		"""
		Method that is called immediately whenever the slider's value is
		changed.
		
		:param sig: signal from the SignalProxy
		:type sig: pyqtSignal
		"""
		self.isChanging = True
		self.lastValue = self.value()
	def delayedChange(self, sig):
		"""
		Method that is called immediately whenever the slider's value is
		changed, but at a specific latency behind the original activity.
		Then it checks if the current signal is the same as the original,
		and if so, it updates its self.isChanging status.
		
		:param sig: signal from the SignalProxy
		:type sig: pyqtSignal
		"""
		if self.value() == self.lastValue:
			self.isChanging = False
			self.valueChangedDelayed.emit(self.value())
	def slowChange(self):
		"""
		Method that is called immediately whenever the slider's value is
		changed, but at a user-defined rate. Then it emits the rate-limited
		signal.
		"""
		self.valueChangedSlow.emit(self.value())
